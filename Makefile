all: pigtail

pigtail: pigtail.c
	$(CC) -o $@ $< `pkg-config --cflags --libs libconfig`

clean:
	rm -f pigtail.o pigtail

